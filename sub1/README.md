# icd6-file-splitter

- Read a file from a S3 bucket (arg [sourceBucket](#markdown-header-sourcebucket-required), arg [sourceKey](#markdown-header-sourcekey-required))
- Separate the file into header and chunks, according to the icd6 format
- Upload those chunks in another S3 bucket (env [CHUNK_BUCKET](#markdown-header-chunk_bucket-required))
- Start one AWS Job (icd6-chunk-processor) for the header and for each generated chunk, passing the S3 bucket and key of the chunk and a flag if is a header as arguments

---

### Expected environment

The **icd6-file-splitter** is designed to run in an AWS infrastructure.  
It uses the same AWS account of the environment where it is running, internally it uses the *DefaultAWSCredentialsProviderChain*.  
It won't work outside an AWS instance, unless you set the required environment variables for the AWS credentials, to do so, please check the [SDK AWS documentation](https://docs.aws.amazon.com/sdk-for-java/v1/developer-guide/credentials.html).

---

### Program arguments

Example:

    java -jar icd6-file-splitter-1.0.jar "my-s3-bucket" "my-icd6-file.zip" "fake-event-id-0"

All arguments are required, so any number of arguments other than '3' causes the program to fail.  

#### These are the expected arguments to run this program:

##### sourceBucket: *(required)*

S3 bucket where the ICD6 files are stored.
 
##### sourceKey: *(required)*

Name of the file in the bucket.
A zip compressed file is expected (with extension .zip), but gz or uncompressed files should work as well.

##### sourceEventId: *(required)*

ID of the event generated in S3 when a file is created, moved or copied to the bucket.
(This value is used only to uniquely name the jobs this program starts, a fake ID should work without major problems)

---

### Environment variables

##### S3_REGION: *(required)*

The region where the S3 buckets are located (both the bucket for incoming files and the bucket for chunks).

A table with Region names and the corresponding 'Region' string that should be used in this configuration can be found in:

https://docs.aws.amazon.com/general/latest/gr/rande.html

##### JOB_REGION: *(required)*

The same of the S3_REGION configuration, but for the AWS Batch jobs that will be started by this process.

##### JOB_QUEUE_NAME: *(required)*

The name of the AWS Job Queue configured for the chunk-processor that will be started to process each chunk.

##### JOB_DEFINITION_NAME: *(required)*

The name of the AWS Job Definition configured for the chunk-processor that will be started to process each chunk.

A specific revision of the Job Definition can be set after the name, for example, if the revision 5 is required: ace-hhps-icd6-chunk-processor-jobDefinition:5.

If no revision number is provided, the latest one is used.

##### CHUNK_SIZE: *(required)*

Defines the maximum number of lines of each chunk being processed.

##### CHUNK_BUCKET: *(required)*

Defines the S3 bucket where the chunks to be processes are stored, these chunks are deleted after execution.

##### LOG_LEVEL: *(optional)*

Defines the level of detail for the logs, the possible values are:

    TRACE
    DEBUG
    INFO
    WARN
    ERROR
    FATAL

Each level log everything of its own level, plus the levels above, for example, if `INFO` is set then `INFO`, `WARN`, `ERROR` and `FATAL` is logged, `TRACE` and `DEBUG` is ignored.

##### FORCE_ERROR: *(optional)*

Setting this env variable to anything other than empty will cause the program to fail as soon as it starts.
Used for testing only.

---

### Setup and deployment

All modules of the icd-file-processor must be built and deployed together.
Please check the instructions in the [parent module](../README.md).

---

### Scenario of example

To better understand how the program works, let's suppose the following scenario: 

#### Inputs

- **Environment variables:**

| Name                 | Value                     |
| ---------------------|---------------------------|
| S3_REGION            | us-east-1                 |
| JOB_REGION           | us-east-1                 |
| JOB_QUEUE_NAME       | my-icd6-job-queue         |
| JOB_DEFINITION_NAME  | my-icd6-job-definition:5  |
| CHUNK_SIZE           | 2                         |
| CHUNK_BUCKET         | my-icd6-chunk-bucket      |

- **A zip file called `my-icd6-file.zip` in the S3 bucket `my-s3-bucket` with the content:**

        VERSION: 5
        ATTRIBUTES: 2
        0000      XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX                                                                                                                1  1    Y2    TARGET                                                                                              1         Y
        0001      YYYYYYYYYYYYYYYYYYYYYYYYYYYYYY                                                                                                                2  1    Y2    TARGET                                                                                              1         Y
        DEMOGRAPHICS: 3
        000000000001        XYZ                           2222222222                        YXX00-000  XXXXXXX     XX   00000000XX00**111**000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
        000000000002        XYZ                           2222222222                        YXX00-000  XXXXXXX     XX   00000000XX00**222**000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
        000000000003        XYZ                           2222222222                        YXX00-000  XXXXXXX     XX   00000000XX00**333**000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000

- **Command:**

        java -jar icd6-file-splitter-1.0.jar "my-s3-bucket" "my-icd6-file.zip" "event-id-1"

#### Expected results

- **3 files are generated and uploaded to the bucket `my-icd6-chunk-bucket`:**

1. *my-icd6-file.zip.header*:

        VERSION: 5
        ATTRIBUTES: 2
        0000      XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX                                                                                                                1  1    Y2    TARGET                                                                                              1         Y
        0001      YYYYYYYYYYYYYYYYYYYYYYYYYYYYYY                                                                                                                2  1    Y2    TARGET                                                                                              1         Y
 
2. *my-icd6-file.zip.demographics.1*:

        000000000001        XYZ                           2222222222                        YXX00-000  XXXXXXX     XX   00000000XX00111000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
        000000000002        XYZ                           2222222222                        YXX00-000  XXXXXXX     XX   00000000XX00222000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000

3. *my-icd6-file.zip.demographics.2*:
    
        000000000003        XYZ                           2222222222                        YXX00-000  XXXXXXX     XX   00000000XX00333000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000

- **3 jobs are started in the queue `my-icd6-job-queue`, using the revision number `5` of the job definition `my-icd6-job-definition`, with the following arguments for each job:**

    1. ["my-icd6-chunk-bucket", "my-icd6-file.zip.header"]
    2. ["my-icd6-chunk-bucket", "my-icd6-file.zip.demographics.1"]
    3. ["my-icd6-chunk-bucket", "my-icd6-file.zip.demographics.2"]
